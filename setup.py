from distutils.core import setup

setup(
    name='model_manager',
    version='0.1.0',
    author='Cary Robbins',
    author_email='crobbins@mwmdigital.com',
    packages=['model_manager'],
    #scripts=['bin/foo.sh'],
    #url='http://pypi.python.org/pypi/model_manager/',
    #license='LICENSE.txt',
    description='Custom model manager for Django.',
    long_description=open('README.txt').read(),
    install_requires=[
        "Django >= 1.1.1",
    ],
)

